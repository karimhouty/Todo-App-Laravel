<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/todo', 'TodoController@index')->name('todo');
Route::post('/todo/create', 'TodoController@store')->name('todo.create');
Route::get('/todo/remove/{id}', 'TodoController@destroy')->name('todo.delete');
Route::get('/todo/show/{id}', 'TodoController@show')->name('todo.show');
Route::post('/todo/update/{id}', 'TodoController@update')->name('todo.update');
Route::get('/tofo/completed/{id}', 'TodoController@completed')->name('todo.completed');