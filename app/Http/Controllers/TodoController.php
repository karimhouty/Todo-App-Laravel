<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Todo;

class TodoController extends Controller
{
    public function index(){
        $todos = Todo::all();
        return view('todo.index', compact('todos'));
    }

    public function store(Request $request){
        $todo = new Todo;
        $todo->todo = $request->todo;
        $todo->save();
        Session::flash('success', 'Your Todo was created');
        return redirect()->back();
    }

    public function destroy($id){
        $todo = Todo::findOrFail($id);
        $todo->delete();
        Session::flash('success', 'Your Todo was deleted');
        return redirect()->back();
    }

    public function show($id){
        $todo = Todo::findOrFail($id);
        return view('todo.update', compact('todo'));
    }

    public function update(Request $request, $id){
        $todo = Todo::findOrFail($id);
        $todo->todo = $request->todo;
        $todo->save();
        Session::flash('success', 'Your Todo was updated');
        return redirect()->route('todo');
    }

    public function completed($id){
        $todo = Todo::findOrFail($id);
        $todo->completed = 1;
        $todo->save();
        Session::flash('success', 'Your Todo was marked as completed');
        return redirect()->back();
    }
}
