@extends('layout')

@section('content')

    <form method="post" action="{{route('todo.update', ['id' => $todo->id])}}">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Your Todo : </label>
            <input type="text" name="todo" class="form-control" value="{{$todo->todo}}">
        </div>
    </form>

@stop