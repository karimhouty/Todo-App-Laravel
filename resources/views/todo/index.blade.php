@extends('layout')

@section('content')

    <div class="card mt-2 mb-2">
        <div class="card-header">Add New Todo</div>
        <div class="card-body">
            <form method="post" action="{{route('todo.create')}}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Your Todo : </label>
                    <input type="text" name="todo" class="form-control">
                </div>
            </form>
        </div>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Todo</th>
                <th>Completed</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($todos as $todo)
                <tr>
                    <td>{{$todo->id}}</td>
                    <td>{{$todo->todo}}</td>
                    <td>
                        <span class="badge badge-info">
                            {{$todo->completed ? "Completed" : "Not Completed"}}
                        </span>
                    </td>
                    <td>
                        @if(!$todo->completed)
                            <a href="{{ route('todo.completed', ['id' => $todo->id]) }}" class="btn btn-primary btn-sm">
                                <i class="material-icons md-18">done_outline</i>
                            </a>
                        @endif
                        <a href="{{ route('todo.show', ['id' => $todo->id]) }}" class="btn btn-success btn-sm">
                            <i class="material-icons md-18">edit</i>
                        </a>
                        <a href="{{ route('todo.delete', ['id' => $todo->id]) }}" class="btn btn-danger btn-sm mr-1">
                            <i class="material-icons md-18">delete</i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@stop
